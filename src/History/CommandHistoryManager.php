<?php

namespace Jakmall\Recruitment\Calculator\History;

use Exception;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class CommandHistoryManager implements CommandHistoryManagerInterface
{
    protected $file_directory;
    protected $latest_directory;
    public function __construct()
    {
        $this->file_directory = __DIR__ . '/../../storage/logs/mesinhitung.log';
        $this->latest_directory = __DIR__ . '/../../storage/logs/latest.log';
    }

    public function findAll(): array
    {
        try {
            $file = file($this->file_directory);
            return $file;
        } catch (Exception $e) {
        }
    }
    public function findAllLatest(): array
    {
        try {
            $file = file($this->latest_directory);
            return $file;
        } catch (Exception $e) {
        }
    }

    public function find($id)
    {
        try {
            $file = file($this->file_directory);
            foreach ($file as $line) {
                $logData = explode(",", $line);
                if ($logData[0] == $id) {
                    return $logData;
                }
            }
        } catch (Exception $e) {
        }
    }
    public function findLatest($id)
    {
        try {
            $file = file($this->latest_directory);
            foreach ($file as $line) {
                $logData = explode(",", $line);
                if ($logData[0] == $id) {
                    return $logData;
                }
            }
        } catch (Exception $e) {
        }
    }
    public function findComposite($id)
    {
        try {
            $file = file($this->file_directory);
            foreach ($file as $line) {
                $logData = explode(",", $line);
                if ($logData[0] == $id) {
                    return $logData;
                }
            }
        } catch (Exception $e) {
        }
    }

    public function findAllComposite()
    {
        try {
            $latest_driver = file($this->latest_directory);
            $file_driver = file($this->file_directory);
            return array_unique(array_merge($file_driver, $latest_driver), SORT_REGULAR);
        } catch (Exception $e) {
        }
    }

    public function log($command): bool
    {
        try {
            $latest_driver = file($this->latest_directory);
            if ($command['type'] == 'CALCULATE') {
                if (count($latest_driver) == 10) {
                    array_shift($latest_driver);
                    file_put_contents($this->latest_directory, $latest_driver);
                }
                if ($this->fileDriver($command)) {
                    return True;
                }
                return False;
            } else if ($command['type'] == 'READ') {
                if ($command['driver'] == 'FILE') {

                    $this->latestDriver($command, $latest_driver);
                }
                return False;
            }
            return False;
        } catch (Exception $e) {

            return False;
        }
    }
    public function latestDriver($command, $file)
    {
        try {

            foreach ($file as $line) {
                if (explode(",", $line)[0] == $command['id']) {
                    return True;
                }
            }
            if (count($file) == 10) {
                array_shift($file);
                file_put_contents($this->latest_directory, $file);
            }
            unset($command['type']);
            unset($command['driver']);



            $log_data =  $this->logBuilder($command) . PHP_EOL;
            file_put_contents($this->latest_directory, $log_data, FILE_APPEND);
            return True;
        } catch (\Throwable $th) {
            return False;
        }
    }
    public function fileDriver($command)
    {
        try {
            unset($command['type']);
            unset($command['driver']);
            $file_driver = file($this->file_directory);
            $latest_driver = file($this->latest_directory);
            $log_data = $this->getId($file_driver) . "," . $this->logBuilder($command) . PHP_EOL;
            file_put_contents($this->file_directory, $log_data, FILE_APPEND);
            file_put_contents($this->latest_directory, $log_data, FILE_APPEND);
            return True;
        } catch (Exception $e) {
            return False;
        }
    }
    public function logBuilder($input)
    {
        return implode(",", array_values($input));
    }
    public function getId($file)
    {
        $id = explode(",", end($file));
        return (int)$id[0] + 1;
    }

    public function clear($id): bool
    {
        try {
            $files = file($this->file_directory);
            $latest = file($this->latest_directory);



            foreach ($files as $line) {
                if ($id == explode(",", $line)[0]) {

                    $remaining_item = array_diff($files, [$line]);
                    file_put_contents($this->file_directory, $remaining_item);
                }
            }

            foreach ($latest as $line) {

                if ($id == explode(",", $line)[0]) {
                    $remaining_item = array_diff($latest, [$line]);
                    file_put_contents($this->latest_directory, $remaining_item);
                    return True;
                }
            }

            throw new Exception("Data not found or deleted in only one driver \n");
        } catch (Exception $e) {
            echo $e->getMessage();
            return False;
        }
    }

    public function clearAll(): bool
    {
        try {
            $files = file($this->file_directory);
            $latest = file($this->latest_directory);

            file_put_contents($this->file_directory, []);
            file_put_contents($this->latest_directory, []);

            return True;
        } catch (Exception $e) {
            echo $e->getMessage();
            return False;
        }
    }
    public function readLatestLog($command, $file)
    {
        foreach ($file as $line) {

            if (str_split($line, 1)[0] == $command["id"]) {
                echo "ADA \n";
                return True;
            }
        }
        return False;
    }
}
