<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Jakmall\Recruitment\Calculator\Handler\CalculateHandler;
use Jakmall\Recruitment\Calculator\Handler\LogHandler;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class DivideCommand extends Command
{
    /**
     * @var string
     */
    protected $signature = 'divide {numbers*}';

    /**
     * @var string
     */
    protected $description = "Divide number";

    public function __construct()
    {
        parent::__construct();
    }
    protected function getType(): string
    {
        return "CALCULATE";
    }
    protected function getDriver(): string
    {
        return "FILE";
    }
    public function handle(CommandHistoryManagerInterface $history): void
    {
        [$description, $result] = CalculateHandler::handle($this->getInput(), "/");
        $log_data = LogHandler::logBuilder($description, $result, $this->getInput());
        $log_data['type'] = $this->getType();
        $history->log($log_data);
        $this->comment(sprintf('%s = %s', $description, $result));
    }

    protected function getInput(): array
    {
        return $this->arguments('numbers');
    }
}
