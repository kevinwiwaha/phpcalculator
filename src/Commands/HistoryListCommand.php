<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Exception;
use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\Commands\CalculateHandler;
use Jakmall\Recruitment\Calculator\Handler\LogHandler;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class HistoryListCommand extends Command
{
    /**
     * @var string
     */
    protected $signature = 'history:list {id?} {--driver=composite}';

    /**
     * @var string
     */
    protected $description = "Add number";


    public function __construct()
    {
        parent::__construct();
    }

    protected function getId()
    {
        if ($this->argument('id') != NULL) {
            return $this->argument('id');
        }
        return NULL;
    }
    protected function getDriverOption(): string
    {
        return $this->option('driver');
    }

    public function handle(CommandHistoryManagerInterface $history): void
    {
        try {

            // Retrieve All Data
            $header = ['ID', 'Command', 'Operation', 'Result'];

            if ($this->getId() == NULL) {
                if ($this->getDriverOption() == 'file') {
                    $this->table($header, LogHandler::rowBuilder($history->findAll()));
                } else if ($this->getDriverOption() == 'latest') {
                    $this->table($header, LogHandler::rowBuilder($history->findAllLatest()));
                } else if ($this->getDriverOption() == 'composite') {
                    $this->table($header, LogHandler::rowBuilder($history->findAllComposite()));
                }
            } else {
                if ($this->getDriverOption() == 'file') {
                    $data = $history->find($this->getId());
                    if ($data[0] == NULL) {
                        printf("NO DATA FOUND \n");
                        die;
                    }
                    $input = [
                        'id' => $data[0],
                        'command' => $data[1],
                        'operation' => $data[2],
                        'result' => (float)$data[3],
                        'type' => "READ",
                        'driver' => "FILE"
                    ];

                    $history->log($input);
                    $this->table($header, [$data]);
                } else if ($this->getDriverOption() == 'latest') {
                    $data = $history->findLatest($this->getId());
                    if ($data[0] == NULL) {
                        printf("NO DATA FOUND \n");
                        die;
                    }
                    $input = [
                        'id' => $data[0],
                        'command' => $data[1],
                        'operation' => $data[2],
                        'result' => (float)$data[3],
                        'type' => "READ",
                        'driver' => "LATEST"
                    ];

                    $this->table($header, [$data]);
                }
            }
        } catch (Exception $e) {
            printf($e->getMessage());
        }
    }
}
