<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\Handler\CalculateHandler;
use Jakmall\Recruitment\Calculator\Handler\LogHandler;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class MultiplyCommand extends Command
{
    /**
     * @var string
     */
    protected $signature = 'multiply {numbers*}';

    /**
     * @var string
     */
    protected $description = "Multiply number";

    public function __construct()
    {
        parent::__construct();
    }
    protected function getType(): string
    {
        return "CALCULATE";
    }
    public function handle(CommandHistoryManagerInterface $history): void
    {
        [$description, $result] = CalculateHandler::handle($this->getInput(), "*");
        $log_data = LogHandler::logBuilder($description, $result, $this->getInput());
        $log_data['type'] = $this->getType();
        $history->log($log_data);
        $this->comment(sprintf('%s = %s', $description, $result));
    }

    protected function getInput(): array
    {
        return $this->arguments('numbers');
    }
}
