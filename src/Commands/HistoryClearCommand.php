<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Exception;
use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\Commands\CalculateHandler;
use Jakmall\Recruitment\Calculator\Handler\LogHandler;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class HistoryClearCommand extends Command
{
    /**
     * @var string
     */
    protected $signature = 'history:clear {id?}';

    /**
     * @var string
     */
    protected $description = "Clear history";


    public function __construct()
    {
        parent::__construct();
    }

    protected function getId()
    {
        if ($this->argument('id') != NULL) {
            return $this->argument('id');
        }
        return NULL;
    }
    protected function getDriverOption(): string
    {
        return $this->option('driver');
    }

    public function handle(CommandHistoryManagerInterface $history): void
    {
        try {

            if ($this->getId() != NULL) {

                if ($history->clear($this->getId()) == True) {
                    printf('Data with ID ' . $this->getId() . " is removed \n");
                }
            }
            if ($this->getId() == NULL) {

                if ($history->clearAll() == True) {
                    printf('All history is cleared ' . "\n");
                }
            }
        } catch (Exception $e) {
            printf($e->getMessage());
        }
    }
}
