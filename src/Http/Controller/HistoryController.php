<?php

namespace Jakmall\Recruitment\Calculator\Http\Controller;

use Illuminate\Http\Request;
use Jakmall\Recruitment\Calculator\Handler\ApiHandler;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class HistoryController extends ApiHandler
{

    public function index(CommandHistoryManagerInterface $history, Request $request)
    {
        try {
            if (empty($request->all())) {
                $data = $history->findAllComposite();
                return $this->responseBuilder($data);
            } else if ($request->driver == 'latest') {
                $data = $history->findAllLatest();
                return $this->responseBuilder($data);
            } else if ($request->driver == 'file') {
                $data = $history->findAll();
                return $this->responseBuilder($data);
            }
            return [
                'message' => 'Driver not found'
            ];
        } catch (Exception $e) {
        }
    }

    public function show(CommandHistoryManagerInterface $history, $id, Request $request)
    {
        try {
            if (empty($request->all())) {
                $data = $history->findComposite($id);
                return $this->responseBuilder(implode(",", $data));
            } else if ($request->driver == 'latest') {
                $data = $history->findLatest($id);
                return $this->responseBuilder(implode(",", $data));
            } else if ($request->driver == 'file') {
                $data = $history->find($id);
                return $this->responseBuilder(implode(",", $data));
            }
            return [
                'message' => 'Driver not found'
            ];
        } catch (Exception $e) {
        }
    }

    public function remove(CommandHistoryManagerInterface $history, $id)
    {
        try {
            $history->clear($id);
            http_response_code(204);
            exit;
        } catch (Exception $e) {
        }
    }
}
