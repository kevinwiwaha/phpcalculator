<?php

namespace Jakmall\Recruitment\Calculator\Http\Controller;

use Illuminate\Http\Request;
use Jakmall\Recruitment\Calculator\Handler\ApiHandler;
use Jakmall\Recruitment\Calculator\Handler\CalculateHandler;
use Jakmall\Recruitment\Calculator\Handler\LogHandler;
use Jakmall\Recruitment\Calculator\History\CommandHistoryManager;

class CalculatorController extends ApiHandler
{
    protected $history;
    public function __construct()
    {
        $this->history = new CommandHistoryManager;
    }
    public function calculate(Request $request, $action)
    {
        $data = [
            'command' => $action,
            'numbers' => json_decode($request->getContent())->input
        ];
        [$description, $result] = CalculateHandler::handle($data, $this->getOperator($action));
        $log_data = LogHandler::logBuilder($description, $result, $data);
        $log_data['type'] = $this->getCalculateType();
        $this->history->log($log_data);

        return $this->getResponse($action, $description, $result);
    }
}
