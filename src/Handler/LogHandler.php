<?php

namespace Jakmall\Recruitment\Calculator\Handler;

use Exception;

class LogHandler
{

    public static function rowBuilder($data_array): array
    {
        $row = [];
        foreach ($data_array as $data) {
            $a = explode(",", $data);
            array_push($row, $a);
        }
        return $row;
    }
    public static function logBuilder($description, $result, $input): array
    {
        $command = $input['command'];
        $log['command'] = ucfirst($command);
        $log['operation'] = $description;
        $log['result'] = $result;
        return $log;
    }
}
