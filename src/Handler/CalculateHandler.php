<?php

namespace Jakmall\Recruitment\Calculator\Handler;

use Exception;

class CalculateHandler
{
    protected static $parameter;
    protected static $operator;


    public static function handle(array $parameter, string $operator)
    {
        self::setOperator($operator);
        self::setParameter($parameter);
        $numbers = self::$parameter;
        $description = self::generateCalculationDescription($numbers);
        $result = self::calculateAll($numbers);
        return [$description, $result];
    }
    protected static function setOperator(string $operator): void
    {
        self::$operator = $operator;
    }
    protected static function setParameter(array $parameter): void
    {
        self::$parameter = $parameter;
    }
    protected static function generateCalculationDescription(array $numbers): string
    {
        $operator = self::getOperator();
        $glue = sprintf(' %s ', $operator);
        return implode($glue, $numbers["numbers"]);
    }

    protected static function getOperator(): string
    {
        return self::$operator;
    }

    /**
     * @param array $numbers
     *
     * @return float|int
     */
    protected static function calculateAll(array $number)
    {
        array_shift($number);
        if (self::$operator == "+") {
            return self::sum($number["numbers"]);
        } else if (self::$operator == "-") {
            return self::subtract($number["numbers"]);
        } else if (self::$operator == "*") {
            return self::multiply($number["numbers"]);
        } else if (self::$operator == "/") {
            return self::divide($number["numbers"]);
        } else if (self::$operator == "^") {
            return self::power($number["numbers"]);
        }
    }
    protected static function sum(array $number): float
    {
        return array_sum($number);
    }
    protected static function subtract(array $number): float
    {
        $result = (float)$number[0];
        for ($i = 1; $i < count($number); $i++) {
            $result -= (float)$number[$i];
        }
        return $result;
    }
    protected static function multiply(array $number): float
    {
        $result = (float)$number[0];
        for ($i = 1; $i < count($number); $i++) {
            $result *= (float)$number[$i];
        }
        return $result;
    }
    protected static function divide(array $number): float
    {
        $result = (float)$number[0];
        for ($i = 1; $i < count($number); $i++) {
            $result /= (float)$number[$i];
        }
        return $result;
    }
    protected static function power(array $number): float
    {
        if (count($number) != 2) {
            throw new Exception('Only 2 digits accepted');
        }
        return pow((int)$number[0], (int)$number[1]);
    }
}
