<?php

namespace Jakmall\Recruitment\Calculator\Handler;

use Illuminate\Http\Request;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class ApiHandler
{

    public function responseBuilder($data)
    {

        try {
            $output = [];
            if (gettype($data) == 'string') {

                $item_array = explode(",",  trim($data));
                $value = [
                    'id' => $item_array[0],
                    'command' => $item_array[1],
                    'operation' => $item_array[2],
                    'input' => array_map('trim', explode($this->getOperator(strtolower($item_array[1])), $item_array[2])),
                    'result' => $item_array[3]
                ];
                return $value;
            }

            foreach ($data as $line) {
                $item = trim($line);
                $item_array = explode(",", $item);
                $value = [
                    'id' => $item_array[0],
                    'command' => $item_array[1],
                    'operation' => $item_array[2],
                    'input' => array_map('trim', explode($this->getOperator(strtolower($item_array[1])), $item_array[2])),
                    'result' => $item_array[3]
                ];
                array_push($output, $value);
            }
            return $output;
        } catch (\Throwable $e) {
        }
    }
    protected function getOperator($action)
    {
        $trimmed_action = trim($action);
        if ($trimmed_action == "add") {
            return "+";
        } else if ($trimmed_action == "subtract") {
            return "-";
        } else if ($trimmed_action == "divide") {
            return "/";
        } else if ($trimmed_action == "multiply") {
            return "*";
        } else if ($trimmed_action == "power") {
            return "^";
        }
        http_response_code(204);
        exit;
    }
    protected function getCalculateType(): string
    {
        return "CALCULATE";
    }
    protected function getResponse($action, $description, $result)
    {
        return [
            'command' => $action,
            'operation' => $description,
            'result' => $result
        ];
    }
}
